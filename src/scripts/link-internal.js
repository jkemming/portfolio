/**
 * allows custom navigation for internal links via JS.
 * upon navigation, a JSON file with significantly smaller size is loaded
 * instead of the usual HTML file.
 * in addition, this allows for animating navigation.
 */
"use strict";


let handleDomElementWillUpdate;
let handleDomElementDidUpdate;
let contentState = CONTENT_DISPLAYING;

/**
 * holds request and timeout references for content loading
 */
let contentLoadingData = null;

/**
 * represents the normal content state, in which it is visible
 */
const CONTENT_DISPLAYING = 0;
/**
 * represents the initial content loading state, in which it fades out and the
 * loading bar at the top of the screen fades in
 */
const CONTENT_LOADING = 1;
/**
 * represents the advanced content loading state, in which the content height is
 * removed and the loading icon appears
 */
const CONTENT_CHANGING = 2;


// HTML element references
let content;
let navItemsWrapper;
let navToggle;


function onInit(args) {
	// only activate if browser supports history API
	if (window.history && window.history.pushState) {
		handleDomElementWillUpdate = args.handleDomElementWillUpdate;
		handleDomElementDidUpdate = args.handleDomElementDidUpdate;

		content = document.querySelector(".content");
		navItemsWrapper = document.querySelector(".nav__items");
		navToggle = document.querySelector("#nav-toggle");

		onDomElementDidUpdate(document);
		window.addEventListener("popstate", onStatePop);
		updateHistoryState();
	}
}

function onDomElementDidUpdate(element) {
	// register event listeners for all internal links
	(element.querySelectorAll("a.internal") || []).forEach((link) => {
		link.addEventListener("click", onLinkClick);
	});
}

function onLinkClick(e) {
	// prevent following the link
	e.stopPropagation();
	e.preventDefault();

	// unfocus clicked link
	document.activeElement.blur();

	// if new path is equal to the current one, do nothing
	const path = e.target.closest("a.internal").getAttribute("href");
	if (path === window.location.pathname) {
		return;
	}

	requestPage({
		state: {
			path: path,
			position: {
				left: 0,
				top: 0
			}
		},
		pushState: true
	});
}

function onStatePop() {
	// load page
	requestPage({
		pushState: false,
		state: history.state
	});
}

function requestPage(args) {
	// abort current request if it exists
	if (contentState === CONTENT_LOADING || contentState === CONTENT_CHANGING) {
		window.clearTimeout(contentLoadingData.enterChangingStateTimeout);
		window.clearTimeout(contentLoadingData.leaveChangingStateTimeout);
		contentLoadingData.request.abort();
	}

	// activate loading state
	setContentState(CONTENT_LOADING);

	handleDomElementWillUpdate(content);

	// set content loading data
	contentLoadingData = {
		enterChangingStateTimeout: window.setTimeout(() => {
			setContentState(CONTENT_CHANGING);

			// if request completed first, render new page
			if (contentLoadingData.requestCompleted) {
				renderPage();
			}

			contentLoadingData.enterChangingStateTimeoutCompleted = true;
		}, 150),
		enterChangingStateTimeoutCompleted: false,
		request: new XMLHttpRequest(),
		requestCompleted: false,
		startTime: Date.now()
	};

	// set new history state
	if (args.pushState) {
		// save current scroll position
		updateHistoryState();

		history.pushState(
			{
				path: args.state.path,
				position: {
					left: args.state.position.left,
					top: args.state.position.top
				}
			}, null, args.state.path
		);
	}

	// deactivate navigation
	navToggle.checked = false;
	navToggle.dispatchEvent(new Event("change"));

	// decativate all active nav items
	(navItemsWrapper.querySelectorAll(".nav__item--active") || []).forEach((activeNavItem) => {
		activeNavItem.classList.remove("nav__item--active");
	});

	// determine request url
	let url;
	if (args.state.path === "" || args.state.path.endsWith("/")) {
		// if an index file needs to be called
		url = `${args.state.path}index.json`;
	} else {
		url = `${args.state.path}.json`;
	}

	contentLoadingData.request.open("GET", url, true);

	contentLoadingData.request.addEventListener("load", () => {
		if (contentLoadingData.request.status >= 200 && contentLoadingData.request.status < 400) {
			// if hideContentTimeout is completed, render new page
			if (contentLoadingData.enterChangingStateTimeoutCompleted) {
				renderPage();
			}

			contentLoadingData.requestCompleted = true;
		} else {
			// if server returns an error code, redirect to the requested URL
			document.location = args.state.path;
		}
	});

	contentLoadingData.request.addEventListener("error", () => {
		// if no connection can be established, redirect to the requested URL
		document.location = args.state.path;
	});

	contentLoadingData.request.send();
}

function renderPage() {
	setContentState(CONTENT_LOADING);

	contentLoadingData.leaveChangingStateTimeout = window.setTimeout(() => {
		const page = JSON.parse(contentLoadingData.request.response);

		// update window title and content
		document.title = page.data.title;
		content.innerHTML = page.html;

		// activate correct nav item
		if (typeof page.data.navItemActive !== "undefined") {
			navItemsWrapper.querySelector(`.nav__item--${page.data.navItemActive}`).classList.add("nav__item--active");
		}

		// restore scroll position
		window.scrollTo(history.state.position.left, history.state.position.top);

		// update modules
		handleDomElementDidUpdate(content);

		// deactivate loading state
		setContentState(CONTENT_DISPLAYING);
	}, 150);
}

function updateHistoryState() {
	// replace history state with the current path and scroll positions
	history.replaceState(
		{
			path: window.location.pathname,
			position: {
				left: document.documentElement.scrollLeft,
				top: document.documentElement.scrollTop
			}
		}, null, window.location.pathname
	);
}

function setContentState(state) {
	contentState = state;
	switch (contentState) {
		case CONTENT_DISPLAYING:
			document.body.classList.remove("content-loading");
			document.body.classList.remove("content-changing");
			break;
		case CONTENT_LOADING:
			document.body.classList.add("content-loading");
			document.body.classList.remove("content-changing");
			break;
		case CONTENT_CHANGING:
			document.body.classList.add("content-loading");
			document.body.classList.add("content-changing");
			break;
		default:
			throw new Error("Unknown content state");
	}
}


module.exports = {
	onInit,
	onDomElementDidUpdate
};
