/*
 * manages all used plugins and delegates initialization and DOM update events
 */
"use strict";

const plugins = [
	require("./label-keytoggle"),
	require("./link-internal"),
	require("./nav-focus"),
	require("./nav-sticky"),
	require("./tooltip")
];

function runPluginHook(hookName, ...hookArgs) {
	for (const plugin of plugins) {
		if (typeof plugin[hookName] === "function") {
			plugin[hookName](...hookArgs);
		}
	}
}

(function () {
	// initialize modules
	runPluginHook("onInit", {
		handleDomElementWillUpdate: element => runPluginHook("onDomElementWillUpdate", element),
		handleDomElementDidUpdate: element => runPluginHook("onDomElementDidUpdate", element)
	});

	// print edgy log message
	console.log("Hey there! If you see this, you probably want to see my code. Why not check out the source code at https://gitlab.com/jkemming/portfolio instead of this bundled up mess?");
})();
