/**
 * allows showing custom tooltips instead of standard, browser-specific implementations.
 */
"use strict";

const ATTRIBUTE_TIMEOUT_ID = "data-timeout-id";
const ATTRIBUTE_TOOLTIP_ID = "data-tooltip-id";
const ATTRIBUTE_TOOLTIP_TITLE = "data-tooltip-title";

const ELEMENT_SELECTOR_TOOLTIP_TRIGGER = ".tooltip-trigger";

const TOOLTIP_MARGIN = 8;

let nextTooltipId = 0;

function onInit() {
	onDomElementDidUpdate(document);
}

function onDomElementWillUpdate(element) {
	(element.querySelectorAll(ELEMENT_SELECTOR_TOOLTIP_TRIGGER) || []).forEach(removeTooltip);
}

function onDomElementDidUpdate(element) {
	(element.querySelectorAll(ELEMENT_SELECTOR_TOOLTIP_TRIGGER) || []).forEach(trigger => {
		trigger.setAttribute(ATTRIBUTE_TOOLTIP_TITLE, trigger.getAttribute("title"));
		trigger.removeAttribute("title");

		trigger.addEventListener("mousemove", onTriggerActivate);
		trigger.addEventListener("focusin", onTriggerActivate);
		trigger.addEventListener("mouseleave", onTriggerDeactivate);
		trigger.addEventListener("focusout", onTriggerDeactivate);
		if (!trigger.classList.contains("tooltip-trigger--clickable")) {
			trigger.addEventListener("click", onTriggerDeactivate);
		}
	});
}

function onTriggerActivate(e) {
	const triggerElement = e.target.closest(ELEMENT_SELECTOR_TOOLTIP_TRIGGER);

	if (triggerElement.getAttribute(ATTRIBUTE_TIMEOUT_ID) === null
		&& triggerElement.getAttribute(ATTRIBUTE_TOOLTIP_ID) === null) {
		triggerElement.setAttribute(ATTRIBUTE_TIMEOUT_ID, window.setTimeout(createTooltip, 400, triggerElement));
	}
}

function onTriggerDeactivate(e) {
	removeTooltip(e.target.closest(ELEMENT_SELECTOR_TOOLTIP_TRIGGER));
}

function createTooltip(triggerElement) {
	const tooltipId = nextTooltipId++;
	const title = triggerElement.getAttribute(ATTRIBUTE_TOOLTIP_TITLE);
	const isFixed = triggerElement.classList.contains("tooltip-trigger--fixed");

	triggerElement.setAttribute(ATTRIBUTE_TOOLTIP_ID, tooltipId);
	triggerElement.removeAttribute(ATTRIBUTE_TIMEOUT_ID);

	const tooltipElement = createTooltipElement(tooltipId, title, isFixed);
	tooltipElement.style.top = calculateTooltipTopPosition(triggerElement, tooltipElement, isFixed);
	tooltipElement.style.left = calculateTooltipLeftPosition(triggerElement, tooltipElement, isFixed);
}

function createTooltipElement(tooltipId, title, isFixed) {
	const tooltipElement = document.createElement("div");

	tooltipElement.id = `tooltip-${tooltipId}`;
	tooltipElement.classList.add("tooltip");
	if (isFixed) {
		tooltipElement.classList.add("tooltip--fixed");
	}
	tooltipElement.style.top = "0";
	tooltipElement.style.left = "0";
	tooltipElement.textContent = title;

	document.body.appendChild(tooltipElement);

	return tooltipElement;
}

function calculateTooltipTopPosition(triggerElement, tooltipElement, isFixed) {
	const triggerRect = triggerElement.getBoundingClientRect();
	const tooltipRect = tooltipElement.getBoundingClientRect();
	const scrollY = isFixed ? 0 : window.scrollY;

	if (triggerRect.top + triggerRect.height + tooltipRect.height + 2 * TOOLTIP_MARGIN > document.documentElement.clientHeight) {
		// bottom screen edge
		return `${scrollY + triggerRect.top - tooltipRect.height - TOOLTIP_MARGIN}px`;
	} else {
		return `${scrollY + triggerRect.top + triggerRect.height + TOOLTIP_MARGIN}px`;
	}
}

function calculateTooltipLeftPosition(triggerElement, tooltipElement, isFixed) {
	const triggerRect = triggerElement.getBoundingClientRect();
	const tooltipRect = tooltipElement.getBoundingClientRect();
	const scrollX = isFixed ? 0 : window.scrollX;

	if (triggerRect.left + triggerRect.width / 2 + tooltipRect.width / 2 + TOOLTIP_MARGIN > document.documentElement.clientWidth) {
		// right screen edge
		return `${scrollX + document.documentElement.clientWidth - tooltipRect.width - TOOLTIP_MARGIN}px`;
	} else if (triggerRect.left + triggerRect.width / 2 - tooltipRect.width / 2 - TOOLTIP_MARGIN < 0) {
		// left screen edge
		return `${scrollX + TOOLTIP_MARGIN}px`;
	} else {
		return `${scrollX + triggerRect.left + triggerRect.width / 2 - tooltipRect.width / 2}px`;
	}
}

function removeTooltip(trigger) {
	const timeoutId = trigger.getAttribute(ATTRIBUTE_TIMEOUT_ID);
	const tooltipId = trigger.getAttribute(ATTRIBUTE_TOOLTIP_ID);

	trigger.removeAttribute(ATTRIBUTE_TIMEOUT_ID);
	trigger.removeAttribute(ATTRIBUTE_TOOLTIP_ID);

	if (timeoutId !== null) {
		window.clearTimeout(timeoutId);
	}

	const tooltipElement = document.getElementById(`tooltip-${tooltipId}`);
	if (tooltipElement !== null) {
		tooltipElement.classList.add("tooltip--fade-out");
		window.setTimeout(tooltipElement => {
			tooltipElement.remove();
		}, 150, tooltipElement);
	}
}

module.exports = {
	onInit,
	onDomElementWillUpdate,
	onDomElementDidUpdate
};
