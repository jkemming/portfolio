/**
 * provides a JS workaround to make nav items stick to the screen when scrolling down far enough,
 * should the browser not natively support `position: sticky`.
 */
"use strict";

// represents the nav items wrapper's current position.
// can have three values:
// `top`: wrapper is positioned at the start of the nav container, which is the normal position
// `fixed`: wrapper is fixed to the top left of the screen
// `bottom`: wrapper is positioned at the end of the nav container, essentially the inverse of `top`
let navPosition;
// indicates if the custom implementation should be used
let active = false;

// HTML element references
let nav;
let navItemsWrapper;

function onInit() {
	nav = document.querySelector(".nav");
	navItemsWrapper = document.querySelector(".nav__items");

	// if position: sticky is not supported, activate JS implementation
	if (window.getComputedStyle(navItemsWrapper).position !== "sticky") {
		active = true;
		navItemsWrapper.classList.remove("nav__items--sticky");
		window.addEventListener("scroll", onScroll);
		updateNav(true);
	}
}

function onDomElementDidUpdate() {
	if (active) {
		updateNav(true);
	}
}

function onScroll() {
	updateNav(false);
}

function updateNav(reset) {
	const navRect = nav.getBoundingClientRect();
	const navItemsWrapperRect = navItemsWrapper.getBoundingClientRect();

	// only check position if the nav items wrapper is smaller than the nav container,
	// this is only true if there"s enough content on the page and it's not in mobile view
	if (reset || navRect.height > navItemsWrapperRect.height) {
		// number of pixels to scroll until the position has to change from "fixed" to "bottom"
		const togglePointBottom = navRect.top + navRect.height - navItemsWrapperRect.height;

		if (navRect.top >= 0 && navPosition !== "top") {
			// if nav container is not above the upper screen border, but the nav items wrapper is still
			// either in "fixed" or "bottom" position
			navPosition = "top";
			navItemsWrapper.classList.remove("nav__items--bottom");
			navItemsWrapper.classList.remove("nav__items--fixed");
		} else if (navRect.top < 0 && togglePointBottom > 0 && navPosition !== "fixed") {
			// if nav container is above the upper screen border, however the toggle point for `bottom` is
			// not yet reached and the nav items wrapper is not in `fixed` position
			navPosition = "fixed";
			navItemsWrapper.classList.remove("nav__items--bottom");
			navItemsWrapper.classList.add("nav__items--fixed");
		} else if (togglePointBottom <= 0 && navPosition !== "bottom") {
			// if the toggle point has been reached, but the nav items wrapper is not in `bottom` position
			navPosition = "bottom";
			navItemsWrapper.classList.remove("nav__items--fixed");
			navItemsWrapper.classList.add("nav__items--bottom");
		}
	}
}

module.exports = {
	onInit,
	onDomElementDidUpdate
};
