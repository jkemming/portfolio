/**
 * controls whether or not `nav__item`s can be focused.
 * if the nav is hidden in mobile view, they should not be focusable.
 */
"use strict";

// HTML element references
let navToggle;
let navItemsList;

// whether the nav items have a tabindex >= 0
let tabActive = true;

function onInit() {
	navToggle = document.querySelector("#nav-toggle");
	navItemsList = document.querySelectorAll(".nav__item");

	navToggle.addEventListener("change", updateTabIndex);
	window.addEventListener("resize", updateTabIndex);
	updateTabIndex();
}

function updateTabIndex() {
	if (!tabActive && (window.innerWidth >= 700 || navToggle.checked)) {
		// if not in mobile view or if mobile nav is open, activate tab index
		setTabIndex(0);
	} else if (tabActive && window.innerWidth < 700 && !navToggle.checked) {
		// if mobile nav is closed, deactivate tab index
		setTabIndex(-1);
	}
}

function setTabIndex(index) {
	tabActive = index >= 0;
	(navItemsList || []).forEach((navItem) => {
		navItem.setAttribute("tabIndex", index);
	});
}

module.exports = {
	onInit
};
