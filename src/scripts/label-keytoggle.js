/**
 * allows checkbox inputs to be toggled by pressing `enter` on their corresponding label(s)
 */
"use strict";

function onInit() {
	onDomElementDidUpdate(document);
}

function onDomElementDidUpdate(element) {
	(element.querySelectorAll("label.key-toggle") || []).forEach((label) => {
		label.addEventListener("keydown", (e) => {
			if (e.keyCode === 13) {
				// if enter was pressed, invert the corresponding input's status
				const input = document.getElementById(label.getAttribute("for"));
				input.checked = !input.checked;
				input.dispatchEvent(new Event("change"));
			}
		});
	});
}

module.exports = {
	onInit,
	onDomElementDidUpdate
};
