'use strict';

const path = require('path');
const pug = require('pug');
const through = require('through2');

// renders a Pug template while making data (passed as a parameter) available in the global `data` object
function render(args) {
	return pug.render(args.template, {
		basedir: path.join(__dirname, '../src/views'),
		buildId: args.buildId,
		filename: args.filePath,
		data: args.data,
		version: args.version
	});
}

// creates a JSON file which contains data such as the window title and active nav item (defined in `/frontend/src/view-config.json`),
// as well as the page HTML content (meaning without the base template)
function createJsonFile(file) {
	// render pug template
	const html = render({
		buildId: file.buildId,
		data: file.data.build,
		filePath: file.vinyl.path,
		template: file.template,
		version: file.version
	});

	// create vinyl file
	const vinyl = file.vinyl.clone();
	vinyl.extname = '.json';
	vinyl.contents = Buffer.from(JSON.stringify({
		data: file.data.run,
		html: html
	}));
	return vinyl;
}

// creates a complete HTML file by wrapping the content template in the base template.
// the HTML files are loaded when navigating without the custom JS navigation
function createHtmlFile(file) {
	// render pug template
	const html = render({
		buildId: file.buildId,
		data: file.data.build,
		filePath: file.vinyl.path,
		template: `extends /base.pug\r\nblock content\r\n\t${file.template.replace(/\n/g, '\r\n\t')}`,
		version: file.version
	});

	// create vinyl file
	const vinyl = file.vinyl.clone();
	vinyl.extname = '.html';
	vinyl.contents = Buffer.from(html);
	return vinyl;
}

module.exports = function (args) {
	return through.obj(function (file, encoding, cb) {
		const data = args.viewConfig[file.stem];
		if (file.isBuffer()) {
			// create files
			const fileConfig = {
				buildId: args.buildId,
				// `build` data is available to Pug during build time, while `run` data is available in the JSON file.
				// this prevents unnecessary data in the JSON file (such as meta tags).
				// all data is sourced from `/frontend/src/view-config.json`, where `shared` data is available both during
				// build and run.
				data: {
					build: { ...data.shared, ...data.build },
					run: { ...data.shared, ...data.run }
				},
				template: file.contents.toString(encoding),
				version: args.version,
				vinyl: file
			};
			const htmlFile = createHtmlFile(fileConfig);
			const jsonFile = createJsonFile(fileConfig);

			// return HTML and JSON files
			this.push(htmlFile);
			this.push(jsonFile);
			return cb();
		} else if (file.isStream()) {
			console.error('stream is not implemented');
		}
	});
};
