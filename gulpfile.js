'use strict';

const browserify = require('browserify');
const del = require('del');
const express = require('express');
const gulp = require('gulp');
const gulpIf = require('gulp-if');
const babel = require('gulp-babel');
const imagemin = require('gulp-imagemin');
const rename = require('gulp-rename');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const uglify = require('gulp-uglify');
const { v4: uuid } = require('uuid');
const source = require('vinyl-source-stream');
const buffer = require('vinyl-buffer');

sass.compiler = require('node-sass');

const pug = require('./gulp/pug');
const viewConfig = require('./src/view-config.json');


// source, watch and destination paths
const paths = {
	src: {
		assets: './src/assets/**/*.{txt,woff2,xml}',
		images: './src/assets/**/*.{jpg,png,svg}',
		scripts: './src/scripts/base.js',
		styles: './src/styles/base.scss',
		views: './src/views/public/**/*.pug'
	},
	watch: {
		assets: './src/assets/**/*.{txt,woff2,xml}',
		images: './src/assets/**/*.{jpg,png,svg}',
		scripts: './src/scripts/**/*.js',
		styles: './src/styles/**/*.{css,scss}',
		views: './src/views/**/*.pug'
	},
	dest: './public/'
};
const prod = process.env.NODE_ENV === 'production';
const buildId = uuid();

function clean() {
	return del([paths.dest]);
}

function assets() {
	return gulp.src(paths.src.assets)
		.pipe(gulp.dest(paths.dest));
}

function images() {
	return gulp.src(paths.src.images)
		.pipe(gulpIf(prod, imagemin()))
		.pipe(rename({ suffix: `-${buildId}` }))
		.pipe(gulp.dest(paths.dest));
}

function scripts() {
	const browserifyConfig = {
		entries: paths.src.scripts,
		debug: prod ? false : true
	};
	return browserify(browserifyConfig).bundle()
		.pipe(source('base.js'))
		.pipe(buffer())
		.pipe(gulpIf(!prod, sourcemaps.init()))
		.pipe(babel({ presets: ['@babel/env'] }))
		.pipe(gulpIf(prod, uglify()))
		.pipe(gulpIf(!prod, sourcemaps.write()))
		.pipe(rename({ suffix: `-${buildId}` }))
		.pipe(gulp.dest(paths.dest));
}

function styles() {
	const sassConfig = {
		outputStyle: prod ? 'compressed' : 'expanded'
	};
	return gulp.src(paths.src.styles)
		.pipe(gulpIf(!prod, sourcemaps.init()))
		.pipe(sass(sassConfig).on('error', sass.logError))
		.pipe(gulpIf(!prod, sourcemaps.write()))
		.pipe(rename({ suffix: `-${buildId}` }))
		.pipe(gulp.dest(paths.dest));
}

// this project uses a custom-made Pug plugin to allow the creation of both HTML and JSON files from the same template.
// for details, please see the `/frontend/gulp/pug.js` file
function views() {
	const pugConfig = {
		viewConfig: viewConfig,
		buildId: buildId,
		version: process.env.VERSION
	};
	return gulp.src(paths.src.views)
		.pipe(pug(pugConfig))
		.pipe(gulp.dest(paths.dest));
}

function watch() {
	const options = {
		events: 'all',
		ignoreInitial: false,
		queue: false
	};
	gulp.watch(paths.watch.assets, options, assets);
	gulp.watch(paths.watch.images, options, images);
	gulp.watch(paths.watch.scripts, options, scripts);
	gulp.watch(paths.watch.styles, options, styles);
	gulp.watch(paths.watch.views, options, views);
}

function serve() {
	const app = express();
	// middleware to disable cache during development
	app.use('/', (req, res, next) => {
		res.set({
			'Cache-Control': 'private, no-cache, no-store, must-revalidate',
			'Expires': '-1',
			'Pragma': 'no-cache'
		});
		next();
	});
	app.use(express.static(paths.dest, { extensions: ['html'] }));
	app.listen(8080);
}


exports.build = gulp.series(clean, gulp.parallel(assets, images, scripts, styles, views));
exports.watch = gulp.series(clean, gulp.parallel(watch, serve));
