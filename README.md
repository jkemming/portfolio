# Overview

This is my personal portfolio website project.
It is built with Pug, SCSS and JS while keeping a set of principles in mind:

* Static Deployment: Deploying the website should not require the use of any dynamic back-end (like Node.js, PHP, etc.).
	This is primarily intended to ease deployment, keep the cost of running the website low and retain flexibility when evaluating deployment options.
* Optional JavaScript: Clients should not be required to have JS enabled as there are reasons for purposefully disabling it.
	This means that all functionality should be usable without JS or, failing that, provide an adequate fallback should JS be unavailable.
* Minimal Size: The resulting files (HTML, CSS, JS, etc.) should be kept small and lightweight.
	Primarily, this results in the disuse of larger CSS and JS frameworks.

# Setup

To develop and build this project, you need to have [Node.js](https://nodejs.org/en/) and [NPM](https://www.npmjs.com/) installed.
As for the editor, it should support [EditorConfig](https://editorconfig.org/), [ESLint](https://eslint.org/) and [stylelint](https://stylelint.io/).

## Development

To develop locally, you must navigate to `/frontend/` and install the NPM dependencies via `npm install` .
Afterwards, you can start a local server via `npm run watch` and view the development build on `http://localhost:8080` .

## Production

To build the project, run `npm run build` .
This creates a static build consisting of just HTML, CSS and JS in the `/public/` folder.

# Deployment

The static build can be deployed in various ways, currently GitLab Pages is used.

# Versioning

Since this project does not provide an API, the use of semantic versioning is not applicable here.
Instead I use a custom pattern, where fixes, smaller features, documentation updates, etc. increase the "patch" version and larger changes as well as collections of smaller changes constitute a change to the "minor" version.
The "major" version is reserved for major site overhauls.

# Commits

As of v1.4.1, commits and merge requests have to use the form of `<type>: <subject>` where `<type>` is one of the following:

* feat (feature)
* fix (bug fix)
* docs (documentation)
* style (formatting, missing semi colons, …)
* refactor
* test (when adding missing tests)
* chore (maintain)

This is adapted from [semantic commits](https://gist.github.com/stephenparish/9941e89d80e2bc58a153).
